# Multipass Demo

## Overview

A MultiPass is a json hash of basic user information encrypted with strong AES encryption. MultiPasses are typically passed as URL params and can be decrypted by the trusted partner using their partner key. 

## Getting started
To get started:

* Open the MultipassDemo.sln solution in Visual Studio 2012 or above.
* Restore the Nuget packages using Visual Studio or via Nuget on the command line (nuget restore).
* Run the MultipassDemo app with the --demo hook on the command line (e.g. MultipassDemo --demo).

## Usage

The MultipassDemo is made up of two parts:

* A console app showing the full encryption/decryption process.
* A sample MVC website showing how you would implement Multipass in your site.

### Console app
Once compiled, the application can be run like so. You can also use --help as an argument to get usage information.

>MultipassDemo --demo

### Sample website
You can also run the website by launching it from VisualStudio and then visiting the following Url to see just the decryption side of the protocol working. The token and keys used in the site were pre-generated using the Demo utility: 

>http://localhost:22076/?token=VTlJGx0gJpUnCY%2FZNENCoznSOEhPfgljzi7IGXFUOoms0S2RTQXuFdDX7L4L086U3%2F9RUwuXRPxdDZMglVLg2P7H4wdsSlvucJuh8XLiwzsuybnOgqVVwmlaPSiOgXWoyDbXXvKFBrQAuE3Z1PXM9w%3D%3D

**Warning: do not ever use the sample website keys in a live application! These are for demo purposes only!**

## Implementation in your solution
If your site is a .NET application you will be able to use the code from the MultipassWeb/Controllers/HomeController.cs as a starting point for your application.

If you are using a different language you will need to build a similar set of methods. Though not an exact implementation of CDSM Multipass, the Java, Node, PHP, Python and Ruby examples in the following Github repo may help you develop your own: https://github.com/assistly/multipass-examples.

If you develop a working example in a different language, feel free to create a pull request to contribute it back for other users to implement.

## The Multipass process
The following diagram shows how the Multipass process works and what responsibilities lie with the source application providing the token and your application:

![IMAGE](https://bitbucket.org/cdsm/multipass-demo/raw/develop/Resources/multipass_process.png)

## Frequently asked questions

### Q. What level of encryption does Multipass use?
A. The Multipass tokens are encrypted using our shared private key and 256 bit AES encryption. This means that should it be intercepted by a third party it is impossible to either inject data as part of a man-in-the-middle attack or decrypt it to steal user data.

### Q. How am I protected from replay attacks?
A. When the Multipass tokens are generated they have an expiry time stamp. This is configurable on the server level but we recommend a starting value of 10 seconds. You will need to validate the expiry time stamp when you receive and decrypt the token to ensure its not a replay attack. You also have the option of storing tokens and allowing them to be used only once.

### Q. What about a man-in-the-middle intercepting and using a user's token the first time it is sent?
A. You need to ensure your Multipass endpoint is running under HTTPS so that the user's browser has to complete a successful HTTPS negotiation with your server. All core browsers will block traffic (or at least warn users) if the HTTPS negotiation has failed due passing through a third party.

## TODO
* Show example of validating token expiry time stamp in the demo

## History

* 1.0.3 - Added multipass process diagram
* 1.0.2 - Added FAQ and TODO section to readme
* 1.0.1 - Refinements from internal review
* 1.0.0 - Demonstration capable solution