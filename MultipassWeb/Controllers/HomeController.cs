using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace MultipassWeb.Controllers
{
    public class HomeController : ApiController
    {
        private static readonly string PARTNER_KEY = ConfigurationManager.AppSettings["CDSM.PartnerKey"];
        private static readonly string SECRET_KEY = ConfigurationManager.AppSettings["CDSM.SecretKey"];

        [Route]
        public async Task<HttpResponseMessage> Get(string token = null)
        {
            if (token == null) return Request.CreateResponse(HttpStatusCode.OK, "Welcome to the Multipass Demo", new XmlMediaTypeFormatter());

            using (var aes = new AesManaged())
            {
                aes.IV = Encoding.UTF8.GetBytes(PARTNER_KEY);
                aes.Key = Encoding.UTF8.GetBytes(SECRET_KEY);

                var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, decryptor, CryptoStreamMode.Write))
                    {
                        var data = Parse(token);
                        await csEncrypt.WriteAsync(data, 0, data.Length);
                        csEncrypt.FlushFinalBlock();
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, Encoding.UTF8.GetString(msEncrypt.ToArray()), new XmlMediaTypeFormatter());
                }
            }
        }

        /// <summary>
        /// Parses the specified encrypted token into a usable byte array.
        /// </summary>
        /// <param name="encryptedToken">The encrypted token.</param>
        /// <returns></returns>
        private static byte[] Parse(string encryptedToken)
        {
            return Convert.FromBase64String(Uri.UnescapeDataString(encryptedToken));
        }
    }
}