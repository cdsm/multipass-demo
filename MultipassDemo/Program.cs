﻿using CommandLine;
using System;

namespace MultipassDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new Options();
            
            if (Parser.Default.ParseArguments(args, options) == false)
            {
                Console.WriteLine("Command line arguments were invalid. Stopped.");
                return;
            }

            if (options.Demo) Utility.Demonstrate();
        }
    }
}
