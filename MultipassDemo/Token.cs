﻿using System;

namespace MultipassDemo
{
    class Token
    {
        /// <summary>
        /// Gets or sets the globally unique identifier for the user this token has been issued to.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the date and time that token should expire. When the token has expired it should be considered invalid.
        /// </summary>
        public DateTimeOffset Expires { get; set; }

        /// <summary>
        /// Gets or sets the globally unique identifier for the organisation that the user belongs to.
        /// </summary>
        public string OrganisationId { get; set; }

        /// <summary>
        /// Gets or sets the role that the user has.
        /// </summary>
        /// <example>Teacher, Staff, Learner, Student, Administrator, Associate</example>
        public string Role { get; set; }
    }
}