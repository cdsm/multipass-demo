﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace MultipassDemo
{
    static class Utility
    {
        /// <summary>
        /// Conducts a MultipassDemo on the command line.
        /// </summary>
        public static void Demonstrate()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Demo starting.");
            Console.ForegroundColor = ConsoleColor.Gray;

            // CDSM will use a salt for generating partner keys. This will be secret to us
            var multipass_salt = Guid.NewGuid().ToString();

            // We will generate a partner key for you
            var partner_key = MakeString(CreateKey(Guid.NewGuid().ToString(), multipass_salt, 64));
            Console.WriteLine("Unique Partner Key: " + partner_key);
            
            // We will generate a secret key for you
            var secret_key = MakeString(CreateKey(partner_key, multipass_salt, 128));
            Console.WriteLine("Unique Secret Key: " + secret_key);

            // We will guarantee that the secret key is reliably generated from the partner key with our salt
            // We may not store your secret key. We may instead generate it when needed. If your partner key changes, so does your secret key.
            // Keys will be invalidated this way, either for a single secret key, or all keys ever issued to you by changing the salt we use.
            ValidatePredictableSecretKeys(partner_key, multipass_salt);

            // This is an example token, we will use the Newtonsoft.Json library for serialization, and we will exclude whitespace and formatting
            // to reduce string sizes and ensure performance is optimal (less text to encrypt/decrypt).
            var test = new Token { Expires = DateTimeOffset.UtcNow.AddMinutes(1), Id = "multipass-demo-user", OrganisationId = "1", Role = "System" };
            var token = JsonConvert.SerializeObject(test, Formatting.None);
            Console.WriteLine("Token: " + token);

            // Encrypt the token
            var encryptedToken = EncryptToken(partner_key, secret_key, token);

            // Decrypt the encrypted version of the token
            var decryptedToken = DecryptToken(partner_key, secret_key, encryptedToken);

            // Print the outputs for verification
            Console.WriteLine();
            Console.WriteLine("Encrypted Token: " + encryptedToken);
            Console.WriteLine();
            Console.WriteLine("Decrypted Token: " + decryptedToken);
            Console.WriteLine();

            // Verify that the decrypted token matches the original
            Console.ForegroundColor = token == decryptedToken ? ConsoleColor.Green : ConsoleColor.Red;
            Console.WriteLine(token == decryptedToken ? "Decrypted token matches original." : "Decrypted token does not match, something went wrong!");
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Demo complete.");
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        /// <summary>
        /// Decrypts the token.
        /// </summary>
        /// <param name="partner_key">The partner_key.</param>
        /// <param name="secret_key">The secret_key.</param>
        /// <param name="encryptedToken">The encrypted token.</param>
        /// <returns></returns>
        private static string DecryptToken(string partner_key, string secret_key, string encryptedToken)
        {
            using (var aes = new AesManaged())
            {
                aes.IV = Encoding.UTF8.GetBytes(partner_key);
                aes.Key = Encoding.UTF8.GetBytes(secret_key);

                var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, decryptor, CryptoStreamMode.Write))
                    {
                        var data = Parse(encryptedToken);
                        csEncrypt.Write(data, 0, data.Length);
                        csEncrypt.FlushFinalBlock();
                    }

                    return Encoding.UTF8.GetString(msEncrypt.ToArray());
                }
            }
        }

        /// <summary>
        /// Encrypts the token.
        /// </summary>
        /// <param name="partner_key">The partner_key.</param>
        /// <param name="secret_key">The secret_key.</param>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        private static string EncryptToken(string partner_key, string secret_key, string token)
        {
            using (var aesAlg = new AesManaged())
            {
                aesAlg.IV = Encoding.UTF8.GetBytes(partner_key);
                aesAlg.Key = Encoding.UTF8.GetBytes(secret_key);

                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        var data = Encoding.UTF8.GetBytes(token);
                        csEncrypt.Write(data, 0, data.Length);
                        csEncrypt.FlushFinalBlock();
                    }

                    return Encode(msEncrypt.ToArray());
                }
            }
        }

        /// <summary>
        /// Validates the secret keys generated are predictable.
        /// </summary>
        /// <param name="partner_key">The partner_key.</param>
        /// <param name="multipass_salt">The multipass_salt.</param>
        private static void ValidatePredictableSecretKeys(string partner_key, string multipass_salt)
        {
            var generatedKeys = new List<string>();
            for (var i = 0; i < 100; i++)
            {
                generatedKeys.Add(MakeString(CreateKey(partner_key, multipass_salt, 128)));
            }

            Console.WriteLine("Secret Key generates reliably?: " + generatedKeys.All(a => a == generatedKeys.First()));
        }

        /// <summary>
        /// Encodes the specified encrypted array to be url friendly.
        /// </summary>
        /// <param name="encryptedArray">The encrypted array.</param>
        /// <returns></returns>
        private static string Encode(byte[] encryptedArray)
        {
            return Uri.EscapeDataString(Convert.ToBase64String(encryptedArray, Base64FormattingOptions.None));
        }

        /// <summary>
        /// Parses the specified encrypted token into a usable array.
        /// </summary>
        /// <param name="encryptedToken">The encrypted token.</param>
        /// <returns></returns>
        private static byte[] Parse(string encryptedToken)
        {
            return Convert.FromBase64String(Uri.UnescapeDataString(encryptedToken));
        }

        /// <summary>
        /// Creates the key.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="salt">The salt.</param>
        /// <param name="keySize">Size of the key.</param>
        /// <param name="iterations">The iterations.</param>
        /// <returns></returns>
        private static byte[] CreateKey(string password, string salt, int keySize = 256, int iterations = 300)
        {
            var keyBytes = keySize / 8;

            var sha1 = SHA1.Create();
            var saltedHash = sha1.ComputeHash(Encoding.UTF8.GetBytes(salt), 0, (salt).Length);
            Array.Resize(ref saltedHash, keyBytes);

            var keyGenerator = new Rfc2898DeriveBytes(password, saltedHash, iterations);
            return keyGenerator.GetBytes(keyBytes);
        }

        /// <summary>
        /// Makes the string.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private static string MakeString(byte[] data)
        {
            var sb = new StringBuilder();
            Array.ForEach(data, a => sb.Append(a.ToString("X2")));
            return sb.ToString();
        }
    }
}