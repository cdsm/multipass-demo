﻿using System;
using System.Diagnostics;
using System.Reflection;
using CommandLine;
using CommandLine.Text;

namespace MultipassDemo
{
    class Options
    {
        [Option("demo", DefaultValue = false, HelpText = "If enabled, will demonstrate the multipass functionality.")]
        public bool Demo { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            var help = new HelpText
                {
                    Heading = new HeadingInfo("Multipass Utility", GetProductVersion()),
                    AddDashesToOption = true,
                    AdditionalNewLineAfterOption = true
                };
            help.AddOptions(this);
            return help;
        }

        public static string GetProductVersion()
        {
            var assembly = Assembly.GetAssembly(typeof(Options));
            if (assembly == null) throw new ApplicationException("MultipassDemo is not available.");

            return FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;
        }
    }
}